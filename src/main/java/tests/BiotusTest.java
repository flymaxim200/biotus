package tests;


import org.classes.MainPage;
import org.classes.ProductPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class BiotusTest extends BrowserStackSetup{
    private WebDriver driver;

    @BeforeTest
    public void setUpBrowserStack() throws Exception {
        BrowserStackSetup browserStackSetup = new BrowserStackSetup();
        browserStackSetup.setUp();
        driver = browserStackSetup.getDriver();

        driver.get(browserStackSetup.baseURL);
    }

    @Test
    public void searchAndAddProductToCart() {
        MainPage mainPage = new MainPage(driver);
        ProductPage productPage = new ProductPage(driver);
        mainPage.fillFieldSearch("vitamin a");
        productPage.checkFilteredVitamin();
        productPage.clickSecondPage();
        productPage.checkFilteredVitaminOnTheSecondPage();
        productPage.addItemToTheCart();
    }
}


