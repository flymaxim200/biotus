package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.URL;
import org.openqa.selenium.chrome.ChromeOptions;

public class BrowserStackSetup {
    public static WebDriver driver;
    public static String baseURL = "https://biotus.ua/";
    public static WebDriver getDriver() {
        return driver;
    }

    public static void setUp() throws Exception {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--enable-notifications");

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("browser", "Chrome");
        caps.setCapability("browser_version", "latest");
        caps.setCapability("os", "OS X");
        caps.setCapability("os_version", "Big Sur");
        caps.setCapability("project", "YourProjectName");
        caps.setCapability("build", "YourBuildName");
        caps.setCapability("name", "TestName");
        caps.setCapability(ChromeOptions.CAPABILITY, options);

        String browserStackUsername = "bsuser_EbCHvt";
        String browserStackAccessKey = "GxZqtoXckYJBNtDKqGyV";
        String browserStackURL = "https://" + browserStackUsername + ":" + browserStackAccessKey + "@hub-cloud.browserstack.com/wd/hub";

        driver = new RemoteWebDriver(new URL(browserStackURL), caps);
    }
}