package org.classes;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class ProductPage extends BasePage{
    @FindBy(xpath = "//h1[@id='page-title-heading']")
    private WebElement textSel;

    @FindBy(xpath = "(//li[@class=' '])[3]")
    private WebElement secondPageBtnSel;

    @FindBy(xpath = "(//button[@title='В кошик'])[2]")
    private WebElement addCartSel;

    @FindBy(xpath = ".amcart-confirm-block")
    private WebElement popCartSel;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public ProductPage clickSecondPage() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", secondPageBtnSel);
        secondPageBtnSel.click();

        return this;
    }

    public ProductPage checkFilteredVitamin() {
        List<WebElement> products = driver.findElements(By.xpath("//div[@class='name-cat']"));
        for (WebElement product : products) {
            Assert.assertTrue(product.getText().contains("Vitamin A"),
                    "Product title contains 'Vitamin A': " + product.getText());
        }
        return this;
    }

    public ProductPage checkFilteredVitaminOnTheSecondPage() {
        List<WebElement> products = driver.findElements(By.xpath("//div[@class='product-name']"));
        for (WebElement product : products) {
            Assert.assertTrue(product.getText().contains("Бета-каротин"),
                    "Product title contains 'Бета-каротин': " + product.getText());
        }
        return this;
    }

    public ProductPage addItemToTheCart() {
        addCartSel.click();
        popCartSel.isDisplayed();
        return this;
    }
}
