package org.classes;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends BasePage{
    @FindBy(xpath = "//input[@id='search']")
    private WebElement searchFieldSel;

    @FindBy(xpath = "//button[contains(text(),'Знайти')]")
    private WebElement searchBtnSel;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public MainPage fillFieldSearch(String itemName) {
        searchBtnSel.isDisplayed();
        searchFieldSel.sendKeys(itemName);
        searchBtnSel.click();

        return this;
    }
}
